 //@author Yassine Ibhir
package lab5.movies.importer;

import java.io.IOException;

public class ProcessingTest  {

	public static void main(String[] args)throws IOException {
	
		String source ="H:\\java 3\\inpu";
		
		String destination ="H:\\java 3\\output";
		
		LowercaseProcessor lower = new LowercaseProcessor(source,destination);
		
		try {
		
			lower.execute();
		}
		
		catch(NullPointerException e) {
			
			System.out.println("source invalid");
		}
		
		catch(IndexOutOfBoundsException e){
			
			System.out.println("no contents in the files. There is not element to remove");
		}
		
		// the source exist now and we are not removing the header. no exception will be thrown.
	
		String source1 = "H:\\java 3\\output";		
		
		String destination1 = "H:\\java 3\\lastOutput";
		
		RemoveDuplicates duplicate = new RemoveDuplicates(source1,destination1);
		
		duplicate.execute();
	}

}
